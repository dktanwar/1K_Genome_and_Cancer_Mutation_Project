#!/usr/bin/perl

use strict;
use warnings;

my $file1 = shift;

my $file2 = shift;

open (my $gene, "bzcat -c $file1 |") || die "Can't open file $file1";								

my %length;																				

while (my $ge_line = <$gene>){															
	chomp $ge_line;
	next if $ge_line =~ /^\#/;
	my @ge_split = split(/\t/, $ge_line);

	$length{$ge_split[2]} = $ge_split[5];											

}
close($gene);

my %seen;

open (my $tcga, "bzcat -c $file2 |") || die "Can't open file $file2";								

while (my $tc_line = <$tcga>){
	chomp $tc_line;
	next if $tc_line =~ /^\#/;
	
	my @tc_split = split(/\t/, $tc_line);

	next unless $tc_split[9] eq "syn";
	
	#my $key = $tc_split[0]."-".$tc_split[1]."-".$tc_split[2]."-".$tc_split[3]."-".$tc_split[4];
	my $key = $tc_split[0]."-".$tc_split[1]."-".$tc_split[2]."-".$tc_split[3];
	

	unless (exists $length{$tc_split[5]}) {
		print STDERR "Could not find the length for $tc_split[5]\n" ;
		next;
	}
	my $my_len = $length{$tc_split[5]};
	if (exists $seen{$key}){
		
		my $a = $seen{$key};

		my @spl_a = split(/\t/, $a);
		
		if ($spl_a[10] >= $my_len) {
			next;

		}else {
			$seen{$key} = $tc_line . "\t" . $my_len;
		}
		
	}else{
		$seen{$key} = $tc_line . "\t" .$my_len;
	}
}

foreach my $print(sort keys %seen){
	print $seen{$print} , "\n";
}

close ($tcga);
