#!/usr/bin/perl

# Checked by Malay on February 9, 2015

use strict;		 # to catch mistakes and aid debugging
use warnings;		 # to catch mistakes and aid debugging 

my $file = shift;		# provided refGene file for hg19 as an input

print join("\t", "#Chromosome", "Gene", "Transcript", "Ts_Stat", "Ts_Stop", "Transcript_Length") , "\n";		# header
open (my $refGene, $file) || die "Can't open $file";		# open the file

while (my $line = <$refGene>){			# read one line at a time
	chomp $line;		# remove the end line character
	next if $line =~ /^\#/;		# go to next line, if encounter '#', as '#' are comments
	my @data = split(/\t/, $line);		# splitting the line into individual fileds (tab-separated)  
	my $transcript_length = $data[5] - $data[4];		# calculating each transcript length
	print join("\t", $data[2], $data[12], $data[1], $data[4], $data[5], $transcript_length) , "\n";			# standard output
}
