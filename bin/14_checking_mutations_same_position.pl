#!/usr/bin/perl

use strict;
use warnings;

#my $file1 = shift;
my $file2 = shift;

open (my $tcga, "bzcat -c $file2 |") || die "Can't open file $file2";

my %seen;
while (my $tc_line = <$tcga>){
    chomp $tc_line;
    next if $tc_line =~ /^\#/;
    
    my @tc_split = split(/\t/, $tc_line);
    next unless $tc_split[9] eq "syn";
    my $key = $tc_split[0]."-".$tc_split[1]."-".$tc_split[2]."-".$tc_split[3];   
#   my $my_len = $length{$tc_split[5]};

    if (exists $seen{$key}){
       	my $a = $seen{$key};
	my @spl_a = split(/\t/, $a);

	if ($spl_a[4] ne  $tc_split[4]) {
	    print $tc_line ,"\n", $a ,"\n";
	}

#else{
#	    $seen{$key} = $tc_line . "\t" . $my_len;
#	}
    }else{
	$seen{$key} = $tc_line;
    }
}
close($tcga);
