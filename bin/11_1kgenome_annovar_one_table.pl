#!/usr/bin/perl

use strict;
use warnings;
use File::Find::Rule;

my $dirname = shift;
my @files = File::Find::Rule->name('*.bz2')->in($dirname);

print join ("\t", "#Sample", "Chromosome",  "Gen_Pos", "Gene",  "Transcript", "AA", "Pos_AA",  "AA_alt", "N", "Pos_N", "N_alt", "Genotype_info", "mutation_type", "Transcript_len"), "\n";

foreach my $file (@files){
    open (my $fh, "bzcat -c $file |") || die "Can't open $file";

    while (my $line = <$fh>){
	chomp $line;
	next if $line =~ /^\#/;
	print $line , "\n"
    }
}
