#!/usr/bin/perl

use strict;
use warnings;

my $file1 = shift;
my $file2 = shift;

print join("\t", "#Sample", "Chromosome", "Gen_Pos", "Gene", "Transcript", "AA", "Pos_AA", "AA_alt", "N", "Pos_N", "N_alt", "Genotype_info", "mutation_type", "Transcript_len") , "\n"; 

open (my $gene, "bzcat -c $file1 |") || die "Can't open file $file1";								

my %length;																				

while (my $ge_line = <$gene>){															
	chomp $ge_line;
	next if $ge_line =~ /^\#/;
	my @ge_split = split(/\t/, $ge_line);
	$length{$ge_split[2]} = $ge_split[5];											
}
close($gene);

my %seen;
open (my $genome, "bzcat -c $file2 |") || die "Can't open file $file2";								

while (my $genome_line = <$genome>){
	chomp $genome_line;
	next if $genome_line =~ /^\#/;
	my @genome_split = split(/\t/, $genome_line);
	next unless $genome_split[12] eq "s";
	my $key = $genome_split[0]."-".$genome_split[1]."-".$genome_split[2];
	unless (exists $length{$genome_split[4]}) {
		print STDERR "Could not find the length for $genome_split[4]\n" ;
		next;
	}
	my $my_len = $length{$genome_split[4]};

	if (exists $seen{$key}){
		my $a = $seen{$key};
		my @spl_a = split(/\t/, $a);
		if ($spl_a[13] >= $my_len) {
			next;
		}else {
			$seen{$key} = $genome_line . "\t" . $my_len;
		}
	}else{
		$seen{$key} = $genome_line . "\t" .$my_len;
	}
}

foreach my $print(sort keys %seen){
	print $seen{$print} , "\n";
}

close ($genome);
