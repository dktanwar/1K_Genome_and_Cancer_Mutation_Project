#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my $file = shift;
open (my $tcga, "bzcat -c $file |") || die "Can't open file $file\n";

my $aa_count;
my %aa;
my %total;
while (my $line = <$tcga>){
	chomp $line;
	print STDERR "[INFO]: $line\n";
	next if $line =~ /^\#/;
	my @split = split(/\t/, $line);

	$aa_count->{$split[0]}->{$split[1]}->{$split[6]}++;
	$aa{$split[6]} = 1;

	$total{$split[0]}->{$split[1]}++;

}

close ($tcga);

my @aa_list = sort keys %aa;

print join("\t", "#Cancer", "Sample", @aa_list , "Total"), "\n";

foreach my $genome (sort keys %{$aa_count}){
	foreach my $id (sort keys %{$aa_count->{$genome}}){
		
		my @line;
		push @line, $genome , $id;

		foreach my $aa (@aa_list) {

			if (exists $aa_count->{$genome}->{$id}->{$aa}) {
			    push @line, $aa_count->{$genome}->{$id}->{$aa};
			}else{
			    push @line, 0; 
			}
		}

		push @line, $total{$genome}->{$id};
		
		print join ("\t", @line), "\n";
	}
}
