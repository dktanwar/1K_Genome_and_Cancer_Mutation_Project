#!/usr/bin/perl

# checked on Feb 3, 2015

use strict;
use warnings;
use File::Find::Rule;
use File::Basename;

my $dirname = shift;
my @genome_files = File::Find::Rule->name('*variant.gz')->in($dirname);

foreach my $genome_files (@genome_files){
	my $file_id = basename($genome_files, ".exonic_variant.gz");

	open (my $fh, "zcat -f $genome_files |") || die "Can't open $genome_files";

	print "#Sample_id\t Chromosome_no\t Genome_Pos\t Gene\t Transcript\t AA\t Pos\t AA_Change\t Type\t\n";

	while (my $line = <$fh>){
		chomp $line;
		next if $line =~ /^\#/;

		my $if_syn = 0;
		my $if_nonsyn = 0;

		my @tags = split(/\t/, $line);
		my $genotype = $tags[18];
		print STDERR "$genotype\n";
		my $ref = undef;
		my $alt = undef;
		if ($genotype =~ /^(\d)\|(\d)\:/) {
			$ref = $1;
			$alt = $2;
		}
		print STDERR "$ref - $alt \n";
		unless (defined $ref || defined $alt) {
			die "Could not parse genotype from $file_id : $line\n";
		}

		if ($alt == 0  && $ref ==0) {
			next;
		}


		next unless $tags[2] eq "SNV";
		$tags[3] =~ s/\,$//;

		my @multi_tags = split (/\,/, $tags[3]);

		foreach my $t (@multi_tags) {
			my @f = split (/\:/, $t);

			foreach my $f (@f) {
				
				my $aa1;
				my $aa_pos;
				my $aa2;
				my $n1;
				my $n2;
				my $n_pos;
				if($f =~ /p\.(\S)(\d+)(\S)/ ){
					$aa1 = $1;
					$aa_pos	 = $2;
					$aa2 = $3;		
				}	
				if ($f =~ /c\.(\S)(\d+)(\S)/ ) {
					$n1 = $1;
					$n_pos = $2;
					$n2 = $3;
				}
				unless ($aa1 && $aa2 && $aa_pos && $n1 && $n2 && $n_pos) {
					die "Could not parse nuc and prot postion from $line\n";
				}

				my @print_line = ($file_id, $tags[4], $tags[5], $f[0], $f[1], $aa1, $aa_pos, $aa2, $n1, $n_pos, $n2);

				if($aa1 eq $aa2){
					$if_syn = 1;
				}
				else{
					$if_nonsyn = 1;
				}

				if ($if_syn) {
					push @print_line, "syn";
				}

				elsif ($if_nonsyn) { 
					push @print_line, "nonsyn";
				}

				print join ("\t", @print_line), "\n";	

			}	

		}
	}
	

	close ($fh);
}
