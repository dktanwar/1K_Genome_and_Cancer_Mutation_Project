#!/usr/bin/perl

use strict;
use warnings;

my $file = shift;
open (my $genome, "bzcat -c $file |") || die "Can't open file $file\n";

my $aa_count;
my %aa;
my %total;
while (my $line = <$genome>){
	chomp $line;
	print STDERR "[INFO]: $line\n";
	next if $line =~ /^\#/;


	my @split = split(/\t/, $line);
	$aa_count->{$split[0]}->{$split[5]}++;
	$aa{$split[5]} = 1;
	$total{$split[0]}++;
}
close ($genome);

my @aa_list = sort keys %aa;
print join("\t", "#Sample_id", @aa_list, "Total") , "\n";
foreach my $genome (sort keys %{$aa_count}){
	my @line;
	push @line, $genome;
	foreach my $aa (@aa_list) {
		if (exists $aa_count->{$genome}->{$aa}) {
			push @line, $aa_count->{$genome}->{$aa};
		}else{
		    push @line, 0;
		}
	}
	push @line, $total{$genome};
	print join ("\t", @line), "\n";
}
