#!/usr/bin/perl

# checked on Feb 3, 2015

use strict;		# to catch mistakes and aid debugging
use warnings;		# to catch mistakes and aid debugging
use File::Find::Rule;		# allows to build rules which specify the desired files and directories
use File::Basename;		# allows to parse filename and suffix
use File::Spec;			# to perform operation of directories or files

my $dirname = shift;		# directory, in which the tcga data is present

opendir(DIR, $dirname) || die "Can't open $dirname";		# open directory

print "#Cancer\t TCGA_id\t Chromosome_no\t Genome_Pos\t Gene\t Transcript\t AA\t Pos\t AA_Change\t Type\t\n";		# header for output file

while (my $dir = readdir(DIR)){			
	next if ($dir =~ m/^\./);		# if directory starts with '.' then skip
	my $cancer_name = $dir;			# inside the input directory, there are sub directories and we are storing the names of the directories as cancer names
	my $cancer = File::Spec->catdir($dirname, $dir);		# concatinate directory and sub-directory
	my @tcga_ex_var = File::Find::Rule->name('*.exonic_variant_function*')->in($cancer);		# find specific file in the folders

	foreach my $tcga_ex_var (@tcga_ex_var){			
		my $basename = basename($tcga_ex_var);		# for each file, extract the basename 
		my $tcga_id;
		if ($cancer_name eq "coad"){		# if the directory name is coad split in differeent manner, because of the different TCGA barcodes
			$tcga_id = (split(/\./, $basename))[0];			# split basename with '.' and take into account only first splitted part
		}else{
			$tcga_id = (split(/\_/, $basename))[0];			# split basename with '_' and take into account only first splitted part
		}
			
		open (my $fh, "zcat -f $tcga_ex_var |") || die "Can't open $tcga_ex_var";		# reading files with zcat function because all files agr gzipped 
		while (my $line = <$fh>){		# for each line
			chomp $line;		# remove the end line character
			next if $line =~ /^\#/;		# go to next line, if encounter '#', as '#' are comments
			my $if_syn = 0;
			my $if_nonsyn = 0;
			my @tags = split(/\t/, $line);			# splitting the line into individual fileds (tab-separated) 
			next unless $tags[2] eq "SNV";			# taking into account only Single Nucleotide Variants
			my @multi_tags = split (/\,/, $tags[3]);		# splitting 4th column of the file with ','
			foreach my $t (@multi_tags){
				my @f = split (/\:/, $t);		# splitting each earlier split with ':'
				foreach my $f (@f){
					if($f =~ /p\.(\S)(\d+)(\S)/ ){			# checking regular expression and accounting only for character, digits and character (one line may have many such expressions)
						my $aa1 = $1;		# (\S)		# amino acid altered
						my $pos = $2;		# (\d+)		# position on the gene
						my $aa2 = $3;		# (\S)		# amino acid altered to
						# $tag[4] = chromosome no., $tag[5] = position in the genome where mutation occurs, $f[0] = gene symbol, $f[1] = Transcript
						my @print_line = ($cancer_name, $tcga_id, $tags[4], $tags[5], $f[0], $f[1], $aa1, $pos, $aa2);		
						
						if($aa1 eq $aa2){		# when amino acids are same at both the positions
							$if_syn = 1;		# assingn 1 to earlier declatered scalar variable
							} else{
								$if_nonsyn = 1;			
							}
							if ($if_syn) {push @print_line, "syn";}			# if the value is 1 in $if_syn, it will print the @print_line and state that its Synonymous mutation
							if ($if_nonsyn) {push @print_line, "nonsyn";}		# if the value is 1 in $if_nonsyn, it will print the @print_line and state that its non-Synonymous mutation
							print join ("\t", @print_line), "\n";			# printing standard output
						}	
					}
				}
			}
			close ($fh);		# close the file
		}
	}
	close(DIR);		# close the directory, provided as input
