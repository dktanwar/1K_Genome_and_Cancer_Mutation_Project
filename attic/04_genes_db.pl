#!/usr/bin/perl

use strict;
use warnings;

my $file = shift;
open (my $fh, "zcat -f $file |") || die "Can't open $file";

my $gene_id_map;

print "#Gene_Sym\t Gene_id\t HGNC\t Transcript\t TS_start\t TS_end\t gene_len\t\n";

while (my $line = <$fh>){

	next if $line =~ /^\#/;
	chomp $line;
	
	my @columns = split(/\t/, $line);
	next unless ($columns[2] eq "mRNA" || $columns[2] eq "gene");

	if ($columns[2] eq "gene") {
		my $parsed_data = parse_line ($columns[8]);
		if (exists $parsed_data->{id}) {
			$gene_id_map->{$parsed_data->{id}} = $parsed_data;
			}else {
				die "Could not parse $line\n";
			}
		}

		if ($columns[2] eq "mRNA") {
			my $gene_len = ($columns[4] - $columns[3]) + 1;
			my $parsed_data = parse_line ($columns[8]);

			die "Could not parse $line\n" unless exists ($parsed_data->{parent});
			die "Could not parse Gene name from $line\n" unless exists ($gene_id_map->{$parsed_data->{parent}});

			my $gene = $gene_id_map->{$parsed_data->{parent}}->{name};
			die "Could not parse $gene\n" unless exists ($gene_id_map->{$parsed_data->{parent}}->{name});
			
			my $transcript = $parsed_data->{name};
			die "Could not parse $transcript\n" unless exists ($parsed_data->{name});

			my $gene_id = $gene_id_map->{$parsed_data->{parent}}->{gene_id};
			die "Could not parse $gene_id\n" unless exists ($gene_id_map->{$parsed_data->{parent}}->{gene_id});

			my $hgnc = $gene_id_map->{$parsed_data->{parent}}->{hgnc} || "-";

			unless ($gene && $transcript && $gene_id && $hgnc) {
				die "Could not  $line\n";
			}

			print join ("\t", $gene, $gene_id, $hgnc, (split(/\./, $transcript))[0], $columns[3], $columns[4], $gene_len), "\n";
		}
	}

	sub parse_line {
		my $column = shift;
		my %return;
		my @first_split = split (/\;/, $column);

		foreach my $i (@first_split) {
			if ($i =~ /\=/) {
				my ($k, $v) = split (/\=/, $i);
				if ($k eq "ID") {
					$return{id} = $v;
				}
				if ($k eq "Parent") {
					$return{"parent"} =$v;
				}
				if ($k eq "Name") {
					$return {"name"} = $v;
				}
				if ($k eq "Dbxref") {
					if ($v =~ /GeneID\:(\d+)/) {
						$return{gene_id} = $1;
					}
					if ($v =~ /HGNC\:(\d+)/) {
						$return{hgnc} = $1;
					}
				}
			}
		}
		return \%return;
	}