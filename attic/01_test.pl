#!/usr/bin/perl

use strict;
use File::Find::Rule;
use File::Basename;
use File::Spec;

my $dirname = '/projects/common/tcga_clean/';

opendir(DIR, $dirname) || die "Can't open $dirname";

while (my $dir = readdir(DIR)){
	next if ($dir =~ m/^\./);
	my $cancer_name = $dir;
	my $cancer = File::Spec->catdir($dirname, $dir);
	my @tcga_ex_var = File::Find::Rule->name('*.exonic_variant_function*')->in($cancer);

	foreach my $tcga_ex_var (@tcga_ex_var){
		my $basename = basename($tcga_ex_var, "_*");
		my $tcga_id = (split("_", $basename))[0];

		my $syn_counts = 0;
		my $non_syncounts = 0;
		
		my %count;
		open (my $fh, "zcat -f $tcga_ex_var |") || die "Can't open $tcga_ex_var";

		while (my $line = <$fh>){
			chomp $line;
			next if $line =~ /^\#/;

			if ((split ' ', $line)[1] eq "nonsynonymous"){
				$non_syncounts += 1;
			}	
			
			elsif ((split ' ', $line)[1] eq "synonymous"){

				$syn_counts += 1;

				my @columns = split(/\s+/, $line);

				foreach my $str  ($columns[3] =~ /\:p\.(\S)(\d+)(\S)\,/ ) {
					die "Synonymous mutation AAs are not same" unless ($1 eq $3);
					# print join ("\t", $cancer_name, $tcga_id,$1, $2, $syn_counts, $non_syncounts), "\n";
					$count {$str}++;
					# print join ("\t", $cancer_name, $tcga_id, $aa, $pos, $syn_counts, $non_syncounts), "\n";
				}
				print %count;
			}	
		}
		close ($fh);
	}
}
close(DIR);