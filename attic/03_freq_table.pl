#!/usr/bin/perl

use strict;
use warnings;

my $file = '../results/01_tcga_cancer/syn_nonsyn_tcga_cancer.txt';


my %count;

open (my $fh, $file) || die "Can't open $file";

while (my $line = <$fh>){
	chomp $line;
	next if $line =~ /^\#/;
	my @tags = split(/\t/, $line);

	next unless $tags[9] eq "syn";

	$count{$tags[6]}++;

}
	print join ("\t", $tags[0], $tags[1], %count), "\n";


close ($fh);