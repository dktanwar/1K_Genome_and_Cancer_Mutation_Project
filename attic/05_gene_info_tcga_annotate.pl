#!/usr/bin/perl

use strict;
use warnings;

my $file1 = '../results/03_trans_gene_db/genes_transcripts_db.txt';
my $file2 = '../results/01_tcga_cancer/syn_nonsyn_tcga_cancer.txt';

open (my $gene, $file1) || die "Can't open file $file1";								# open gene_descp file

my %length;																				# hast to store length of the genes

while (my $ge_line = <$gene>){															
	chomp $ge_line;
	next if $ge_line =~ /^\#/;
	my @ge_split = split(/\t/, $ge_line);

	$length{$ge_split[3]} = $ge_split[6];												# store values into hash

}
close($gene);

open (my $tcga, $file2) || die "Can't open file $file2";								# open mutation data file

while (my $tc_line = <$tcga>){
	chomp $tc_line;

	next if $tc_line =~ /^\#/;
	
	my @tc_split = split(/\t/, $tc_line);												# split the lines

	my @len_gen;																		# array to store mutation data and 
	if (exists $length{$tc_split[5]}){													# check for transctipt in hash created
		
		my $len_gen = join ("\t", $tc_line, $length{$tc_split[5]});						# attach length of a transcript
		
		push @len_gen, $len_gen; 														# pust to the array 
	}

	my @final_file;

	foreach my $l(@len_gen){															
		my @s = split(/\t/, $l);

		next unless $s[9] eq "syn";

		foreach my $cancer($s[0]){
			
			foreach my $sample($s[1]){
				
				foreach my $chromosome($s[2]){
				
					foreach my $gen ($s[4]){
				
						foreach my $trans ($s[5]){
				
							foreach my $position($s[7]){
				
								foreach my $len_trans ($s[10]){
				
									if ($trans eq $trans++ && $position = $position++ && $len_trans > $len_trans++){						# if transcript and next transcript are same & position are same select the length which is greater
				
										my $z = join("\t", $cancer, $sample, $chromosome, $s[3], $gen, $trans, $s[6], $position, $s[8], $s[9], $len_trans);
										push (@final_file, $z);
										}elsif($trans eq $trans++ && $position = $position++ && $len_trans < $len_trans++){
				
											my $z = join("\t", $cancer, $sample, $chromosome, $s[3], $gen, $trans, $s[6], $position, $s[8], $s[9], $len_trans++);
											push (@final_file, $z);
											}else{
				
												my $z = join("\t", $cancer, $sample, $chromosome, $s[3], $gen, $trans, $s[6], $position, $s[8], $s[9], $len_trans);
												push (@final_file, $z);
											}
										}
									}
								}
							}				
						} 
					}
				}
			}
			print @final_file, "\n";
		}
		close($tcga);