#!/usr/bin/perl

use strict;
use warnings;

my $file = shift;
open (my $tcga, "bzcat -c $file |") || die "Can't open file $file\n";

my %aa_count;

while (my $line = <$tcga>){
	chomp $line;
	next if $line =~ /^\#/;

	my @split = split(/\t/, $line);
	$aa_count{join("\t", $split[0], $split[1], $split[6])}++;
}

foreach my $print(sort keys %aa_count){
	print $print, "\t", $aa_count{$print}, "\n";
}