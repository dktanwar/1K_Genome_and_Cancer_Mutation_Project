#!/bin/sh

script=../../bin/11_aa_distribution_1kgenome.pl 
echo ${script}
result_dir=/home/deepak/tcga_clean_1kgenome_project/results/08_1kgenome_aa_distribution
echo ${result_dir}
for i in `find ../06_1k_genome_annovar/ -name "*.genotypes.txt.bz2"`
	do echo $i 
	file=`basename $i`
	outfile=${file}
	qsub -b y -e error.log -o log.log -cwd "${script} ${data} $i | lbzip2 -9 -c > ${result_dir}/${outfile}"
done
