#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

my $file1 = '../results/04_annovar_hg19_refGene_table/refGene_annovar_hg19_table.txt';

my $file2 = shift;
# my $file2 = '../results/01_tcga_cancer/syn_nonsyn_tcga_cancer.txt';

open (my $gene, $file1) || die "Can't open file $file1";								

my %length;																				

while (my $ge_line = <$gene>){															
	chomp $ge_line;
	next if $ge_line =~ /^\#/;
	my @ge_split = split(/\t/, $ge_line);

	$length{$ge_split[5]} = $ge_split[8];											

}
close($gene);

my %seen;

open (my $tcga, $file2) || die "Can't open file $file2";								

while (my $tc_line = <$tcga>){
	chomp $tc_line;
	next if $tc_line =~ /^\#/;
	
	my @tc_split = split(/\t/, $tc_line);

	next unless $tc_split[9] eq "syn";
	my $key = $tc_split[0]."-".$tc_split[1]."-".$tc_split[2]."-".$tc_split[3]."-".$tc_split[4]."-".$tc_split[5];
	
	my $my_len = $length{$tc_split[5]};

	unless ($my_len) {
		print STDERR "Could not find the length for $tc_split[5]\n" ;
		next;
	}

	if (exists $seen{$key}){
		
		foreach my $a (values %seen){ 

		 	my $a = $seen{$key};

			my @spl_a = split(/\t/, $a);
		
			if ($spl_a[10] >= $my_len) {
				next;

			}else {
				$seen{$key} = $tc_line . "\t" . $my_len;
			}
		}

	}else{
		$seen{$key} = $tc_line . "\t" .$my_len;
	}
}
# print Dumper \%seen;
foreach my $print(sort keys %seen){
	print $seen{$print} , "\n";
}

close ($tcga);